# Dufils_PhysRevLett_123_195501_2019

Contains input files and data used to generate the figures of the article: 

Simulating Electrochemical Systems by Combining the Finite Field Method with a Constant Potential Electrode

Thomas Dufils, Guillaume Jeanmairet, Benjamin Rotenberg, Michiel Sprik, and Mathieu Salanne, 
*Phys. Rev. Lett.*, 123, 195501, 2019

https://doi.org/10.1103/PhysRevLett.123.195501 ([see here for a preprint](https://arxiv.org/abs/1907.00622))

The folder *typical_input_files* contains 2 input files for MetalWalls, which is available [here](https://gitlab.com/ampere2/metalwalls)

The folder *figures_data* contains 5 files with the data used to plot Figures 2.a, 2.b and 3
